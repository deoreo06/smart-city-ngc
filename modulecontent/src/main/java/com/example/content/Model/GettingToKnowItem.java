package com.example.content.Model;

/**
 * Created by M. Asrof Bayhaqqi on 6/8/2016.
 */
public class GettingToKnowItem {
    private String title;
    private String date;
    private int thumbnail;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}
