package com.example.content.Controller;

import android.location.LocationManager;

/**
 * Created by Lenovo on 22/06/2016.
 */
public class AppData {
    public static double latitudeOffice = -7.9337192;
    public static double longitudeOffice = 112.6518156;
    public static double myLatitude;
    public static double myLongitude;
    public static LocationManager locationManager;
}
